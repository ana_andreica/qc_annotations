from qc_ui.scripts.checker_ui.abstract_checker_ui import AbstractCheckerUI
import streamlit as st 


class SimpleCheckerUI(AbstractCheckerUI):

    def run(self, project_path):
        st.write(f"Hello from Simple checker => {project_path}")