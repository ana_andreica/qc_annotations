def build_grups_of_consec_img(data_df):
    result_list = []
    
    for sequence_id, seq_df in list(data_df.groupby('sequence_id')):
    
        seq_df = seq_df.sort_values('sequence_index')
        row_list = [r for _,r in seq_df.iterrows()]
        tuple_list = list(zip(row_list, row_list[1:]))
        cons_tuple_list = [t for t in tuple_list if (t[1]['sequence_index'] == t[0]['sequence_index'] + 1) and
                           (t[0]['status'] == 'done' or t[0]['status'] == 'reviewed') and
                           (t[1]['status'] == 'done' or t[1]['status'] == 'reviewed')]
    
        for r1, r2 in cons_tuple_list:
            result_list.append((r1['name'], r2['name']))
            
    return result_list


def get_not_consec_img(group_list, data_df):
    exclude_name_set = set() 
    
    for n1, n2 in group_list:
        exclude_name_set.add(n1)
        exclude_name_set.add(n2)

    excluded_df = data_df[data_df['name'].apply(lambda name: name not in exclude_name_set)]
    excluded_df = excluded_df.drop(['id', 'project', 'image_path', 'annotations', 'sequence_id', 'sequence_index', 'status', 'tags', 'comments'],axis=1)

    return excluded_df


def export_to_csv(not_consec_img_df, export_path):

    not_consec_img_df.to_csv(export_path, index=False) 