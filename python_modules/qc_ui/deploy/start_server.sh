#!/usr/bin/env bash
set -e
CONFIG_FILE="./config.json"

echo 'Parameters:'
echo 'CONFIG_FILE='$CONFIG_FILE
echo '----------------------------------------'

PYTHONPATH=../../:$PYTHONPATH
export PYTHONPATH

streamlit run ../scripts/app.py
