from qc_ui.scripts.checker_ui.abstract_checker_ui import AbstractCheckerUI
import pandas as pd
import streamlit as st 
import base64
from common import utils
from checkers.consec_img import consec_checker


class ConsecImgCheckerUI(AbstractCheckerUI):

    def get_table_download_link(self, df, df_name):
        """Generates a link allowing the data in a given panda dataframe to be downloaded
        in:  dataframe
        out: href string
        """
        csv = df.to_csv(index=False)
        b64 = base64.b64encode(csv.encode()).decode()  # some strings <-> bytes conversions necessary here
        href = f'<a href="data:file/csv;base64,{b64}" download="{df_name}.csv" >Dwonload {df_name}.csv</a>'
        return href

    def run(self, project_path):
        st.write(f"Consecutive images from => {project_path}")

        json_data = utils.read_json(project_path)

        df = utils.json_2_data_df(json_data)

        group_list = consec_checker.build_grups_of_consec_img(df)

        unique_img_names = set()

        for item_1, item_2 in group_list:
            unique_img_names.add(item_1)
            unique_img_names.add(item_2)

        list_df = pd.DataFrame(unique_img_names, columns=["name"])

        st.write(list_df.shape)
        st.write(list_df.head())

        # download link
        st.markdown(self.get_table_download_link(list_df, "results from qc"), unsafe_allow_html=True)