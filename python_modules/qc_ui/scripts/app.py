import streamlit as st
import logging
from glob import glob
import os
from common import utils
from qc_ui.scripts.checker_ui.simple_checker_ui import SimpleCheckerUI
from qc_ui.scripts.checker_ui.complicated_checker_ui import ComplicatedCheckerUI
from qc_ui.scripts.checker_ui.consec_img_checker_ui import ConsecImgCheckerUI
from qc_ui.scripts.checker_ui.not_consec_img_checker_ui import NotConsecImgCheckerUI


class StreamlitApp():

    def __init__(self, config):
        self.config = config
        self.checker_dict = self.get_checker_dict()

    def get_checker_dict(self):
        checker_dict = {
            "Simple Checker": SimpleCheckerUI(),
            "Complicated checker": ComplicatedCheckerUI(),
            "Consecutive images": ConsecImgCheckerUI(),
            "Not Consecutive image": NotConsecImgCheckerUI()
        }

        return checker_dict

    def show_checker_selection(self):
        base_folder = "/data/DA_team_files/annotations_data/"
        project_full_paths = glob(os.path.join(base_folder, "*.json"))
        project_names = sorted([os.path.basename(path) for path in project_full_paths])

        selected_project = st.sidebar.selectbox('Select Project name:', project_names)

        selected_checker = st.sidebar.selectbox("Select the checker:", list(self.checker_dict.keys()))

        return os.path.join(base_folder, selected_project), selected_checker

    def run_app(self):
        st.title("QC App")
        st.sidebar.header("Menu")

        selected_project, selected_checker = self.show_checker_selection()

        st.write(f'You selected: {selected_project}')
        st.write(f'You selected: {selected_checker}')

        selected_checker_ui = self.checker_dict[selected_checker]
        
        selected_checker_ui.run(selected_project)



        # host_percentage = st.sidebar.selectbox('Select the percent:',('1%', '2%', '3%', '4%', '5%', '10%'))
        # st.write('You selected:', host_percentage)

        # json_path = st.sidebar.text_input("Choose the project path", value="/data/DA_team_files/billboards_data/gt_latitude_billboards_annotation_data.json")

        # json_data = utils.read_json(json_path)

        # df = utils.json_2_data_df(json_data)

        # st.write(df.head())

        # # download link
        # st.markdown(self.get_table_download_link(df[['name','id']], "results from qc"), unsafe_allow_html=True)

        # name = st.text_input("What is your name")
        # age = st.text_input("What is your age", value=None)
        
        # st.write(f"{name} and {age}")

        # if st.button("click me"):
        #     st.write(f"{name} and {age}")

        # if st.checkbox("this is a checkbox"):
        #     st.write("hello form checkbox")

        # st.write("hello from streamlit")


def run_component():
    config_path = "./config.json"
    # read config json_read
    StreamlitApp({}).run_app()


if __name__ == '__main__':
    # disable Azure logs
    logger = logging.getLogger("azure.core.pipeline.policies.http_logging_policy")
    logger.setLevel(logging.WARNING)

    run_component()
