from qc_ui.scripts.checker_ui.abstract_checker_ui import AbstractCheckerUI
import pandas as pd
import streamlit as st 
import base64
from common import utils
from checkers.not_consec_img import not_consec_checker


class NotConsecImgCheckerUI(AbstractCheckerUI):

    def get_table_download_link(self, df, df_name):
        """Generates a link allowing the data in a given panda dataframe to be downloaded
        in:  dataframe
        out: href string
        """
        csv = df.to_csv(index=False)
        b64 = base64.b64encode(csv.encode()).decode()  # some strings <-> bytes conversions necessary here
        href = f'<a href="data:file/csv;base64,{b64}" download="{df_name}.csv" >Dwonload {df_name}.csv</a>'
        return href

    def run(self, project_path):
        st.write(f"Not Consecutive images from => {project_path}")

        json_data = utils.read_json(project_path)

        df = utils.json_2_data_df(json_data)

        group_list = not_consec_checker.build_grups_of_consec_img(df)
        not_consec_list = not_consec_checker.get_not_consec_img(group_list, df)

        not_consec_list_df = pd.DataFrame(not_consec_list, columns=['name'])   

        st.write(not_consec_list_df.shape)
        st.write(not_consec_list_df.head())

        # download link
        st.markdown(self.get_table_download_link(not_consec_list_df, "results from qc"), unsafe_allow_html=True)

        # in progress'


