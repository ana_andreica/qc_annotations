import pandas as pd

PHOTO = "photo"
NAME = "name"
STATUS = "status"
ANNOT_LIST = "annot_list"
ANNOT = 'annot'

ID = "id"
CAT = "cat"
STATUS = "status"
TEXT_VALUE = "text_value"
HAS_SYMBOL = "has_symbol"
IS_DONE = "is_done"


def is_done(status):
    return status == "done" or status == "reviewed"


def has_symbol(text_value):
    for i in range(len(text_value)):
        if text_value[i] > u'\u4e00' and text_value[i] < u'\u9fff':
            return True
        else:
            return False
        

def json_2_data_df(content):
    data_df = pd.DataFrame({PHOTO: content})
    data_df[NAME] = data_df[PHOTO].apply(lambda photo: photo["name"])
    data_df[STATUS] = data_df[PHOTO].apply(lambda photo: photo["status"])
    data_df[ANNOT_LIST] = data_df[PHOTO].apply(lambda photo: photo["annotations"])
    data_df = data_df[data_df[ANNOT_LIST].apply(lambda al: len(al) != 0)]
    data_df = data_df.explode(ANNOT_LIST).rename(columns={ANNOT_LIST: ANNOT})
    data_df[ID] = data_df[ANNOT].apply(lambda annot: annot["id"])
    data_df[CAT] = data_df[ANNOT].apply(lambda annot: annot["category"]) 
    data_df[TEXT_VALUE] = data_df[ANNOT].apply(lambda annot: annot["text_value"]) 
    data_df = data_df[data_df[TEXT_VALUE].apply(lambda text_value: text_value is not None)]
    data_df[IS_DONE] = data_df[STATUS].apply(is_done)
    data_df[HAS_SYMBOL] = data_df[TEXT_VALUE].apply(has_symbol)
    
    data_df = data_df.drop(['photo', 'annot'], axis=1)
    
    return data_df


def export_to_csv(data_df, export_path):
    
    symbol_df = data_df[data_df[HAS_SYMBOL]][[NAME, ID, CAT, TEXT_VALUE, IS_DONE]]
    
    symbol_done_df = symbol_df[symbol_df[IS_DONE]]

    symbol_done_df.to_csv(export_path, index=False)
