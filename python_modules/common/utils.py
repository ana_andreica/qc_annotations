import json
import pandas as pd


def read_json(path):
    with open(path) as f:
        content = json.loads(f.read())
    return content


def json_2_data_df(content):
    data_df = pd.DataFrame.from_dict(content)
    
    return data_df

