import pandas as pd


def is_done(status):
    return status == "done" or status == "reviewed"


def json_2_data_df(content):

    PHOTO = "photo"
    NAME = "name"
    STATUS = "status"
    ANNOT_LIST = "annot_list"
    ANNOT = 'annot'
    ID = "id"
    CAT = "cat"
    IS_DONE = "is_done"
    TARGET_CATEGORIES = ["POI_PANEL", "POI_PANEL_PARTIALY_VISIBLE", "POI_PANEL_OTHER", "POI_PANEL_AUXILIAR", "POI_PANEL_AUXILIAR_PARTIALY_VISIBLE", "POI_PANEL_NOT_SURE"]

    data_df = pd.DataFrame({PHOTO: content})
    data_df[NAME] = data_df[PHOTO].apply(lambda photo: photo["name"])
    data_df[STATUS] = data_df[PHOTO].apply(lambda photo: photo["status"])
    data_df[ANNOT_LIST] = data_df[PHOTO].apply(lambda photo: photo["annotations"])
    data_df = data_df.explode(ANNOT_LIST).rename(columns={ANNOT_LIST: ANNOT})
    data_df[ID] = data_df[ANNOT].apply(lambda annot: annot["id"])
    data_df[CAT] = data_df[ANNOT].apply(lambda annot: annot["category"])
    data_df[IS_DONE] = data_df[STATUS].apply(is_done)

    for target_category in TARGET_CATEGORIES:
        data_df[f'HAS_{target_category}'] = data_df[CAT].apply(lambda cat: cat == target_category)

    data_df = data_df.drop(['photo', 'annot'], axis=1)
    
    return data_df


def export_to_csv(data_df, export_perc):

    NAME = "name"
    ID = "id"
    CAT = "cat"
    IS_DONE = "is_done"
    TARGET_CATEGORIES = ["POI_PANEL", "POI_PANEL_PARTIALY_VISIBLE", "POI_PANEL_OTHER", "POI_PANEL_AUXILIAR", "POI_PANEL_AUXILIAR_PARTIALY_VISIBLE", "POI_PANEL_NOT_SURE"]

    for target_category in TARGET_CATEGORIES:
        cat_df = data_df[data_df[f'HAS_{target_category}']][[NAME, ID, CAT, IS_DONE]]

        export_df = cat_df.sample(frac=export_perc)

        export_df.to_csv(f"/data/DA_team_files/outputs/{target_category.lower()}_category_gt_billboards.csv", index=False)    